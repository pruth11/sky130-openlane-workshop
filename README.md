# A Complete flow from RTL->GDSII using OpenLANE.
![Semantic description of image](/images/advanced_physical_design.png )
## Advanced Physical Design - OpenLANE Workshop

<details>
  <summary markdown="span">Table of Contents</summary>

1. [ About the Project](#about)

2. [ Prerequisites ](#prereq)

3. [ A top level RTL to GDSII review](#toplevel)

4. [ Why OpenLANE?](#openlane)

    * [OpenLANE flow](#flow)

    * [Open EDA Tools](#edatools)

5. [ Workshop Introduction](#wrkshop)

    * [ Day1 Insights into Open source EDA tools and Synthesis of an RTL Design](#day1)

    * [ Day2 Lets plan the chip floor and look at Standard Cells including Placement](#day2)

    * [Day3 Standard Cell Design and characterisation along with inclusion into our design](#day3)

    * [ Day4 Timing Analysis of the Layout and Clock Tree Synthesis](#day4)


    * [  Day5 Final generation of RTL --> GDSII](#day5)



</details>



<a name="about"></a>
## About the Project

Imagine you build a chip on your own just like building a house.Oh Yes!!  it will be possible soon. This is what this workshop is all about. It gives a complete overview of the open source chip development flow using OpenLANE.

OpenLANE is an automated RTL to GDSII flow which uses several open source tools like OpenROAD, Yosys, Magic,OpenPhySyn, SPEF-Extractor and scripts for exploration of design space and optimization strategies. With the help of efabless,the goal of OpenLANE is to produce chips with clean GDSII files. Skywater's 130nm open-source PDK is the foundry file which will be used to produce macros and chips.


<a name="prereq"></a>
## Prerequisites
OpenLANE flow can be easily installed locally on your laptops/PC with the below requirements. 
1. Ubuntu based OS or Virtual Box.
2. Disk space of 25GB.

A detailed explanation of installing can be found here:https://github.com/nickson-jose/openlane_build_script
<a name="toplevel"></a>
## A top level RTL to GDSII review

The RTL to GDSII flow is the main backbone of an IC design. Due to continuous scaling of CMOS technologies, the flow undergoes significant changes. It also depends on the type of chip being manufactured. In general, the entire flow can be split into the following steps with two groups:

***FRONT END DESIGN:***
1. Specification: This is a step where idea/request from a customer emerges and the company plans to bring into reality.

2. Architectural Design: A system architect is the one who plans the entire chip/SOC in a most innovative way through his way of implementations and also approximates the device behaviour, performance.An inital datasheet for a device will be generated. This step is the most important and provides inputs to the RTL Design engineers to design the required circuit based on the requirements and constraints.

3. Register Transfer level(RTL) Design: In the early years of IC design, every implementation was manually done by diving into characteristics of each transistor. As device sizes shrinked, working at transistor level was no longer possible and that how this abstraction led to the inclusion of Hardware programming languages. In this step, an engineer implements the design with the help of Hardware description Language(HDL). VHDL, Verilog and System Verilog are the main languages which are used in the industry now. Using HDL, an engineer need not worry which devices to use and just concentrate on what his design must do. Various tools are used which does the mapping of the design to Hardware.
Also, there are different ways to model a design:

    * Behavioral : In this model, you just need to code how your design must behave and not worrying about the implementation.

      Example:
      General: Imagine you have an Automated Driving Car. You just tell the car to travel from location A to location B. 
      Technical: Addition of three numbers.

    * Structural : In this model, you describe which all devices you use and connect them hierarchically. Basically you are implementing an bigger circuit using smaller well known circuits.

      Example:
      General: Now you tell the car to travel from location A to location B via the way you define. 
      Technical: Full Adder Implementation using XOR,OR and AND gates.

    * Switch Level: In this model, you work close to transistor level.

      Example:
      General: Car used in Manual mode and you drive. 
      Technical: Implementation of an AND gates using switches(via pmos, nmos).

4. RTL Verification: After the design, verification of the implemented functionality is the most important step. Due to complexties of today's design, extensive verification is very important for a design to be sent forward to further steps. A basic idea is that a testbench is written which provides stimulus to the design and checks for correctness. Different ways of verification are present in current industry.

    * Testbench based as explained above.

    * Assertion based testbenches: The expected outputs is included in the tool itself by the engineer and the tools checks the outputs and informs about the correctness.

    * Universal Verification Methodology(UVM): This is the latest and most advanced form of verification. The framework for this verification is very well defined and also can be reused for many designs. Seperate Verifcation engineers work on these in parallel with the RTL design Team to reduce the time to market of a device.

5. Design for Testability(DFT) : This step is an advancement to check each and every flop in a device after fabrication before the device is into market. Scan chain Insertion, Memory Built In Self Test(MBIST), Logic Built In Self Test(LBIST) etc are some of the concepts used to include logic in the design itself so that verification after fabrication becomes easy.

***BACK END DESIGN:***

6. Synthesis: This is the process of converting logic to gates. With the help of Electronic Design Automation(EDA) tools, this step is carried out and the output is called Netlist which contains all the connectivity information of the circuit. It also maps those gates to actual technology-dependent logic gates available in TECH libraries from the foundry. The designer gives the design constraints, RTL files and Tech libraries as inputs.Reports are also generated along with netlist which gives information about the area,power and timing.

    * Area report: Gives information about the area used by the cells. Area report gives seperate values for Buffers, Combinational area and Registers. All these information are obtained from the tech file provided as inputs which has width and height details of an individual cell.

    * Power report: Gives information about the total power that will be used by the cells. All macros,Combinational cells and register power utlisation are included. The power is also split into Internal Power, Switching Power, Leakage power for better understanding of the utilsation.

    * Timing report: The most important is to satisfy timing of any design else the design won't work as intended. Static Timing Analysis(STA) is a step which is performed on the design. Setup time is checked on different groups:
    
      a)Reg2Reg path

      b)In2Reg path

      c)Out2Reg path
      
      Any timing violation will appear in terms of slack. This slack must be positive before you go to next step.

      ![Semantic description of image](/images/synthesis.PNG )

7. Floor Planning: In this step, the overall area of the chip which will be used in market is decided.This is similar to how we plan the construction of a house. Various blocks and their positions are decided in this step. Due to stringent time to market, many of the blocks are reused from other projects. For example: A memory block, clock gating cell etc will be designed before and just included in the project where it is needed. These are called macros or IP blocks from various vendors. In Floorplanning, these blocks are preplaced at required positions and then further steps are carried out.

      A chip has two area defined which are Core Area and the Die Area. Die Area is the overall chip area and core is where the     logic cells are placed. The area between Core and the Die are reserved for pin placements of the chip. This area is reserved and logical blockage is placed(also called IO fill) just to make sure the tools further won't place any logic in that area.

      Power Planning: This is also a step during the floorplan which creates the power distribution network(PDN) to power each components of the design. In this, a power ring (also called core ring) is created which are connected to VDD and VSS pads. Power straps are also created so that the logical cells draw the required supply in the vicinity which reduces the RL effect of the wires.Best power planning results in less heat dissipation, less IR drop and also avoids Electon Migration(EM) problems.

      ![Semantic description of image](/images/pp.PNG )

8. Placement: After the PDN is created, all the standard cells are placed on the avalable rows in the floorplan area. Placement is carried out in two steps: Global and Detailed.

      In Global placement(Fast placement), the tool's speed of placing the cells are high and just tries to find optimal positions for the cells. This is soft placement as the blocks are not fixed to the positions. There can be slight overlap or misalignment when placing between rows. Then a detailed placement takes over the global placement and fixes all the problems related to placement adhering to the rules of the global placement.

9. Clock Tree synthesis(CTS): Clock is the most important signal in any chip as it defines the working of all registers in the design. The main aim of any design is to have clock latency zero so that all the registers in the design recives the clock at the same time without any variations. If any timing mismatch, then registers might latch the data at different times resulting in wrong functionality. Bringing to the clock signal just randomly from the clock pads to all registers is a bad way of using clock as the registers might be spread all the chip and having different routing distances results in timing offsets due to different RC values of the wire. So, there is requirement to develop a tree which routes to the clock to all registers such that the skew is almost zero. In this step, clock buffers are introduced into the clock path to ensure signal integrity and better driving capability. H Tree, X trees etc are some of the algorithms used.

10. Routing: In this step, all the blocks which are placed are now connected by physical wires. The timing analysis which was done till now are with ideal routing(zero delays with all connections). Now we have to do the timing analysis once to confirm that real wire delays will not cause any timing violations. When performing routing all the Design Rule checks(DRC) are enabled which ensures proper routing so that it can be fabricated without any issues. There are many tools and they use many algorithms to connect the blocks in the most efficient way with minumum bends and minimum delays.
Other checks: Signal Intergity , Cross talk , Detailed power analysis, Diode insertions to remove Antenna effect of metals and many more are some of the few extreme checks which are used now a days. 

11. Post STA and Graphic Data System(GDSII) extraction: After routing is finished, we do Timing checks and ensure it meets the requirements. Finally, the GDSII extraction is done. This is the input to the foundry where they start the Fabrication of the chip. Many checks are done again before this sign off to ensure the correctness in design.

      The Skywater 130nm PDK used here allows 6 metal layers to perform routing. Right now , the OpenLANE flow doesn't allow other check mentioned above except Antenna checks and will be added over the years.

      The flow can be represnted as:

      ![Semantic description of image](/images/abc.png )
      

<a name="openlane"></a>
## Why OpenLANE?

VLSI field is very costly. As we all know, the EDA tools which are used in the market are from Cadence, Synopsys, Mentor Graphics anad few others. All these tools license costs for usage are huge and often results in smaller companies having own Design flows and tools from other companies. For a chip to be manufactured as an Open source, you need Open source RTL designs, Open source EDA tools, Open source PDKs from the foundries. RTL Designs are available in github and many other platforms. All that was missing was the tools and the PDKs.  OpenLANE is an open source flow which uses all the Open source tools available today to carry out the above steps. Anyone who has great knowledge in this field can try to build their own chip with the help of these tools available now.

<a name="flow"></a>
## OpenLANE flow

The flow followed by the OpenLANE is as shown below. All the names in the brackets are the tools used to carry out the respective step. For the Physical Design steps, OpenROAD application is used which performs the steps as shown below. This flow is advanced and it allows Design Space Exploration and Synthesis exploration where you can perform the steps in different strategies and choose the best possible way of implementation. The goal of this flow is to produce a clean GDSII file with no human in loop.

![Semantic description of image](/images/openlaneflow.png )

<a name="edatools"></a>
## Open EDA Tools
The Open EDA tools which are used in the flow are:

1. Yosys: Synthesis tool for converting the design  into netlist( consists of gates and connections)

2. abc: Sequential synthesis tools for mapping the technology libraries to the standard cells.

3. Fault: This tool is used to insert the DFT flow into the design. Post Silicon Validation becomes easy because of this.

4. OpenSTA: Used for timing Analysis of the design. It gives detailed output of the critical path which helps the designers to alter their design if any timing violations.

5. Triton Route: This tool used for routing the connections in the design. Different versions are available and uses Lee's Algorithm by creating a routing grid and doing the routing.
    
6. DEF2SPEF:  This is used to extract the parasitics from the design which are very important for delay calculation and ensure signal integrity.

7. Magic: This is a layout tool for GDSII extraction which will be given as the final file to the foundry. All the DRC,LVS rules are embedded in the tool.
    
8. Netgen: This along with the Magic can be used to physically verify the design before signing off to the foundry.
    
<a name="wrkshop"></a>
## Workshop Introduction
This workshop was divided into five days to get clear hands on experience on the above mentioned tools. The RTL design of picorv32a was done before and the HDL files were used as the input to this workshop and from there all the steps were carried out dilligently using the OpenLANE flow.

In the early days of IC design, the design process was integrated with manufacturing in each company. So, there was no common flow followed. In 1979, Conway and Mead seperate the Design from manufacturing and lambda(λ) based rules were introduced. This led to the development of Fabless and Fab only companies and all the information related to manufacturing was included in Process Design Kit(PDK).

PDK is a collection of files which gives the manufacturing information for the EDA tools. It includes:
* DRC, LVS, PEX information.
* Device characterisation models (SPICE).
* Digital Standard Cell Libraries.
* I/O Libraries. 

All these files are closed source and hence difficult to get access to. But now Google with collaboration from Skywater has released 130nm open source PDK which can be used by the designers and try to develop their own chips. The workshop uses these files and Open source tools to get full flow information.

<a name="day1"></a>
## Day1 Insights into Open source EDA tools and Synthesis of an RTL Design
### Main folder
The main folder consists of openlane flow and pdk folders
![Semantic description of image](/images/mainfolder.PNG )
### Skywater PDK
The files which are used for the PD flow are included in the OpenLANE working directory as shown below.

![Semantic description of image](/images/pdkfolder.PNG )
The folders include:
1. Skywater-pdk: This contains all the pdk files of the 130nm foundry.
2. open_pdks: This contains scripts which are used to convert the close source pdk from the skywater folder to pdk which is comapatible with the openLANE flow. Seperate scripts are found for each open source tools used as shown below.
![Semantic description of image](/images/insideopenpdks.PNG )

3. sky130A: This contains open-source compatible PDK files.
### OpenLANE folder
The openlane directory consist of the following folders:
1. Design folder consists of all the RTL designs which will be used for synthesis.
2. Configuration folder consists all variables explanations and other files related to tools.
3. Flow.tcl is the script used to run the Openlane flow.
4. Scripts folder consists of tcl scripts related to tools. 
![Semantic description of image](/images/openlanefolder.PNG )

### Design folder
This lists all design available.
![Semantic description of image](/images/designfold.PNG )

We used the picorv32a rtl implementation which contains config.tcl and runs folder which holds all results and reports of any tool run. The techfile config file also contains few configurations and has the highest priority.
![Semantic description of image](/images/pico32.PNG )

Inside config.tcl: 
![Semantic description of image](/images/configtcl.PNG )
This contains the path to the design and SDC files along with clock information.

### Invoking OpenLANE
flow.tcl script is used with interactive flag so that the steps can br executed in a sequential manner for better understanding.
After invoking we include the package of openlane. Many versions available and we used 0.9 one
![Semantic description of image](/images/packagereq.PNG )

### Preparing Design
Prep is used to setup the file structure required for the design. It merges .lef and .tlef into merged.lef.
* Technology LEF(.tlef) contains metal layer definitions and also few design rules needed for PnR flow. 
* Library Exchange Format(.lef) contains information related to standard cell which will be used during PnR flow.
![Semantic description of image](/images/prep.PNG )

After completing the preparation, a new folder called dayone is created in the runs folder of the picorv32a design.
This folder contains few folders as shown below:
![Semantic description of image](/images/dayonefolder.PNG )
The results folder contains seperate folder for each tool output as shown:
![Semantic description of image](/images/resultfolder.PNG )

### Synthesis
Preparing once again will not alter the dayone folder which was created before. This can be overwritten by using the overwrite flag. Used when any changes in config scripts or design.
![Semantic description of image](/images/nooverwrite.PNG )

**run_synthesis** is the command used to run synthesis.
![Semantic description of image](/images/lastfewlines.PNG )
Slack should always be positive. Any negative values means there is a timing violation and needs to be fixed before going to next steps.

After successful completion of the synthesis, timing reports can be found in reports folder under synthesis sub folder.
![Semantic description of image](/images/reportfolder.PNG )
You can also find the Flop ratio and Buffer Ratio from the reports.
```
Flop Ratio = #of D Flip Flops/Total cells

Buffer ratio =  #of Buffers/Total cells
```
<a name="day2"></a>
## Day2 Lets plan the chip floor and look at Standard Cells including Placement
Floorplanning is the first step in Physical Design flow and includes:
1. Setting the die area.
2. Setting the core area based on netlist area.
3. Finding the Core Utilization factor and Aspect Ratio.
4. Power Distribution Network(VDD and VSS rails)
5. Placing Hard IP blocks and Decoupling capacitors.
6. Placing IO pins according to the SOC requirements.

### Setting Die and Core area
Core Area is decided depending on the Area of the Netlist used. For example, lets say you use an AND gate and this gate will be present a blackbox in the library with a width and height using which area can be obtained. In similar way you approximate the area of the core based on your design. Die area is slightly bigger and core area is a part of it. The area between the core and die are reserved for IO pins and no logical cells are placed.

### Core Utilization Factor(CUF) and Aspect Ratio(AR)
Core Utlization is given by the ratio of Netlist Area and the core area. Usually, CUF will be around 70-80% with additional area utlized during routing and any cells to be replaced by Engineering change order(ECO) cells.

Aspect ratio is the ratio of height by width of the die area. If AR=1 then the die is a square. Any other value will be a rectangle.

### Power Distribution Network(VDD and VSS rails)
PDN is the mesh of VDD and VSS rails (rows and columns) used for faster access of supply to the cells. All the standard cells will be placed in between the rows. Due of this, all cells will have fixed height and varying width(multiples of unit width). In openLANE, this is done after the CTS stage which is changed in the upcoming versions.

### Placing Hard IP blocks and Decoupling capacitors
Hard IP's or Macros are the cells which are pre designed and just added to your design. Due o stringent time to market conditions, many designs are made up of IP blocks from different vendors. These blocks pre placed on to the Floorplan and their positions are fixed before the other logic is placed. These helps the routing tools to avoid the area of preplaced cells for routing area utilization and tries to route from other possiblities.However, the routing tool can route above the macros with layers which are not used in macros.

Decoupling capacitors are basically used to supply the charges to the preplaced cells when most of the logic switch in the macros. It acts like a reservoir and avoid the burden on the main supply. Without its use, there will be ground bounce and supply droop which affects the signal strength and might cause wrong results. Since it “decouples” the circuit from the main supply, it is called Decoupling Capacitors.

### Placing IO pins
IO pin placement is very important as it affects the buffer used, timing delays, signal integrity and cross talk issues. After placing the pins, a logical blockage(IO fill) is placed all around to block the area specifically for pins.

## Floorplanning in OpenLANE

The configuration folder contains tcl scripts of individual steps which can be used for controlling the tools.
![Semantic description of image](/images/configfolder.PNG )

Before running the Floorplan, some basic parameters and their details can be checked in README.md file as shown below
![Semantic description of image](/images/readmefile.PNG )

The horizontal and vertical metal layers can also be changed and the defaults used by the tools are always plus one layer defined by you.
![Semantic description of image](/images/fpdefaults.PNG )

To run floorplan in OpenLANE flow
*** run_floorplan *** command is used and the tool runs with the setting set in config.tcl script in the design folder.
![Semantic description of image](/images/fplastlines.PNG )

After the run, you can check the ioplacer.log file in the logs folder which tells the details of the horizontal and vertical layers. 
![Semantic description of image](/images/ioplacer.PNG )

The floorplan output is in the form of Design exchange format(.def) file in the results folder as shown
![Semantic description of image](/images/def.PNG )

The def file consists of all details about the area and standard cell site information.
![Semantic description of image](/images/deffile.PNG )
Area can be calculated by multiplying the two values in the above image
```
Die Area = (1057235/1000)*(804605*1000) //1000 used to convert to microns
```
### Viewing the floorplan
To view the floorplan, Magic tool is used and we must input the tech lef, merged lef and the latest def file which was created as shown
![Semantic description of image](/images/toviewfp.PNG )
![Semantic description of image](/images/fp.PNG )

Snippet of zoomed floorplan  
![Semantic description of image](/images/zoomedfp.PNG )

### Placement
After the floorplan, next step is to run placement as shown below      
![Semantic description of image](/images/runplace.PNG )

After the instruction execution, congestion related placement(less congestion now) is done and you can check the console where you see the overflow value, For a proper placement, this value should converge to zero.

### To view the Placement
The command is similar to when you view floorplan except the def file is now in placement folder.
![Semantic description of image](/images/placement.PNG )

Zoomed version
![Semantic description of image](/images/zoomplace.PNG )


<a name="day3"></a>
## Day3 Standard Cell Design and characterisation along with inclusion into our design 

In day 3 we will understand how a basic standard cell (an Inverter) is designed in Layout tool and characterized by extracting the .lef file. Later we include this into the OpenLANE flow. 

### Standard Cell Design Flow
Lets say we have a design with PnR done. In that design we have gates, buffers,registers etc and all these are called Standard cells. These cells are kept in the library. In library, not only cells with different functionality exists but also with different sizes and different threshold voltage(Vt) too.

A typical cell design flow is as shown below

![Semantic description of image](/fromday3/designflow.PNG )

User defined lib includes supply voltage, metal layers to be used, pin locations and gate length info(poly layer) in layout.

The design steps includes circuit design and simulation, layout design and simulation. Different characterisations like Power, Noise and Timing are carried out and the results are saved which will be used later.

The outputs are usually the LEF file, Spice netlist(.cir) and circuit description.

### Standard Cell characterisation Flow
The spice netlist contains all extracted R and C values and the flow is built based on the input we have. The steps include:
1. Take model files from foundry.
2. Extract the spice netlist.
3. Behaviour of buffers.
4. Read sub circuits of device you are building.
5. Power suplly decide.
6. Apply stimulus.
7. Varying Output capacitance and checking the results.
8. Transient, DC analysis etc are carried out.

All these steps are carried out by a open source software called GUNA.

### Timing characterisation
These include measuring:
1. Rise time: Time required to rise from 20% to 80% of the signal.
2. Fall time: Time required to fall from 80% to 20% of the signal.
3. Propagation delay: Time difference between 50% of output to 50% of the input.

These can be found in CCS and ECS libraries.

Before going further, you can also try floorplan and placement with different modes and settings. The OpenLANE flow allows on the fly changes and run the tools.

### Let's design a Library cell

Spice deck creation is required before spice simulations which acts as a wrapper for the model files created. It contains:
1. Connectivity of all components.
2. Component values.
3. Identity of nodes and substrate connections.

After the deck is created, we need to edit some parameters and include model files. Later, we plot the waveforms using this file on ngspice tool.
Steps include:
1. Sourcing the deck file.
2. Run
3. Set the plot which tells the current plot present in simulator.
4. Select the plot.
5. Display
6. Plot out vs in.
 
Once you get the waveform, check for different W/L ratios of pmos and rerun to see the effect on waveforms. This will explain the reason for bigger pmos than nmos in CMOS technologies. (Hint: Holes are slower than electrons)

### Switching Threshold(Vm)

In the waveform, plot a line of 45 degrees and the point where the waveform and the line meets is the switching threshold. At this point Vin = Vout.

### CMOS manufacturing process 
The latest CMOS manufacturing process is very complex and takes place with 16 masks usage for each deposition steps. The steps are briefly explained as below:
1. Selecting a substrate on which the devices are formed. Next is to create isolation between regions so nmos and pmos doesn't interfere.
2. Deposition of SiO2 and Si3N2 is carried out which is followed by photoresist layer. Mask 1 is used as protection layer for active regions and UV light is passed.
3. Exposed regions to UV are washed away. Si3N2 is etched off at extra points and later photoresist is removed.
4. Oxidation of the substrate is done at 900-1000C so that area not under Si3N2 grows which forms the seperation for the devices.
5. Next is nwell and pwell formations and the process is called the twin tub process. Mask 2 and Mask 3 are used here.
6. To increase the depth of wells, again heated in furnace(4-6 hrs at 1100C) which leads to diffusion and pockets are created.
7. Now Doping(Boron for ptype and Phosphorous for ntype) is used to create the active regions for a transistors. Mask 4 and Mask 5 are used here. Then gate is formed with Mask 6.
8. Lightly Doped Drain(LDD) formation is carried out to avoid hot electron effect and short channel effects.
9. Mask 7 and 8 are used for P- and N- region formation.
10. Deposition of thick oxide is carried out and plasma anisotropic etching is carried out which preserves the deposits at the walls. These are called Sidewall spacers which preserves the lightly doped regions intact.
11. Now p+ and n+ regions are obtained. Thin oxide layer are used to avoid channeling.
12. Mask 9 and 10 are used for source and drain formations which is followed by High temperature Annealing.
13. Further masks are used for metal layer depositions for connections of devices to the outer layers for the functionality.

### Lab Work
Refer to: https://github.com/nickson-jose/vsdstdcelldesign for cell files.

The design of the standard layout was used from the github by cloning the files:
![Semantic description of image](/fromday3/fullgitclone.PNG )
vsdstdcelldesign folder is added with all the required files

Next is to copy the sky130nm tech file into the above folder
![Semantic description of image](/fromday3/cptechlib.PNG )

### Viewing the Layout
To view the layout via Magic the below command is used:
![Semantic description of image](/fromday3/layoutview.PNG )
![Semantic description of image](/fromday3/layout.PNG )

The labelled layout information is as shown below:
![Semantic description of image](/fromday3/namingslayout.PNG )

The tools has many features where you see all the layer information with varying colours and also you can just select an area in the cell and type what in magic console and it gives all the details
![Semantic description of image](/fromday3/whatallseletced_3S.PNG )

DRC checks are automatic in Magic, any issues the designer can check in tabs and find the DRC errors.

### PEX Extraction from the Layout
Extracting the parasitics file from the layout is done by a series of command as shown below:
![Semantic description of image](/fromday3/extract.PNG )

Grid information is as shown:
![Semantic description of image](/fromday3/grid.PNG )

Then the spice file is generated and to this models from the library is included along with addition of Power supply, Input source and ground information. Pulse input is used for transient analysis.
![Semantic description of image](/fromday3/spicefile.PNG )

An example of pshort.lib is as shown below. The model name is pshort_model and contains all the information related to pmos.
![Semantic description of image](/fromday3/pshortlib.PNG )

Next is to run the spice file using ngspice tool 
![Semantic description of image](/fromday3/ngspice.PNG )

To plot the waveform the command used is:
![Semantic description of image](/fromday3/plot.PNG )
![Semantic description of image](/fromday3/wf.PNG )
The waveform shown above is input and output vs time. Using this waveform, rise time, fall time and propagation delays calculations are carried out.

### Rise time
Red waveform is used to calculate the rise time. The difference between the values shown below gives the Rise time.
![Semantic description of image](/fromday3/risetime.PNG )

### Fall time
Red waveform is used to calculate the fall time. The difference between the values shown below gives the Fall time.
![Semantic description of image](/fromday3/fd.PNG )

### Propagation delay
Difference between 50% of both the waveforms. The difference between the values shown below gives the delay.
![Semantic description of image](/fromday3/realrisedelay.PNG )

<a name="day4"></a>
## Day4 Timing Analysis of the Layout and Clock Tree Synthesis
Lets get the .lef file from layout and include it in OpenLANE. For PnR tool, it doesn't need all layout info except for the boundaries and  supply rails.

### Important things to check in Layout
1. IO pins must be at the intersection of Horizontal and Vertical tracks.
2. Width must be an odd multiples of track width(X pitch).
3. Height are usually constant. But can also be an odd multiples in few cases.

The information about the tracks can be found in track.info as shown:
![Semantic description of image](/fromday3/tracksinfo.PNG )

These info are used during routing stage which gives details about the x pitch and y pitch of any layer. This details are measured from the origin.

### Standard Cell Pin Placement
The grid in the currrent layout has 0.01*0.01um and needs to be changed to information from the above track info of li1 and metal1.The command is as shown:
![Semantic description of image](/fromday3/gridchange.PNG )

Grid view to ensure our pin placement is correct at the intersection.
![Semantic description of image](/fromday3/3boxes.PNG )

All pins must be converted to ports before generating the .lef file. This is done as shown:
![Semantic description of image](/fromday3/portA.PNG )

Give a name --> size --> layer to be used --> port enable and press okay. Similarly do it for the other pins.

For output port
![Semantic description of image](/fromday3/porty.PNG )

To set the ports we need to use commands as shown:
![Semantic description of image](/fromday3/portset.PNG )

### Writing the LEF file
The command used is **lef write** and as shown:
![Semantic description of image](/fromday3/lefwrite.PNG )

View of LEF file with the details of the pins as seen below:
![Semantic description of image](/fromday3/leffile.PNG )

Next is to copy the .lef file to the src folder of the design
![Semantic description of image](/fromday3/cptosrc.PNG )

### Now let us include the cell into the OpenLANE flow.

After characterization using GUNA, we get the list of libraries of all corners is as shown:
![Semantic description of image](/fromday3/libraries.PNG )

These above libraries need to be added in the config.tcl file:
![Semantic description of image](/fromday3/configchanges.PNG )

Now we need to invoke OpenLANE with overwrite flag when preparing the design:
![Semantic description of image](/fromday3/overwrite.PNG )

![Semantic description of image](/fromday3/overwriting.PNG )

Before running synthesis, we need to add the .lefs as shown:
![Semantic description of image](/fromday3/runsyn.PNG )

In the synthesis logs, you can see the latest cell included:
![Semantic description of image](/fromday3/vsdinvcheck.PNG )

After running synthesis, we see a huge slack violation of 15.96ns and needs to be made zero or positive to go into further steps:
![Semantic description of image](/fromday3/slackvio.PNG )

Snip of the timing report
![Semantic description of image](/fromday3/timingrpt.PNG )

Fanout of 5 seen for many cells
![Semantic description of image](/fromday3/fanout5.PNG )

### Fixing the slack

Any slack violations must be removed. In the pre CTS, STA analysis is done to check all the setup time violations are removed. The STA provides the detailed report of the slack with worst negative slack (WNS) and total negative slack (TNS). Fixing slack violations can be done by using OpenSTA. You can also peek into the configuration files and also Synopsys design constraints(.sdc) files in the design.

Lets start by first checking the area:
![Semantic description of image](/fromday3/areabfr.PNG )

Now we first change the Synthesis strategy to 2 which is for timing optimization. Before it was 1 (Area opt).
![Semantic description of image](/fromday3/synthstrategy.PNG )

You can check these information in README.md
![Semantic description of image](/fromday3/synvariables.PNG )

We also allowed automatic sizing(size up/down) of buffers which was disabled by default:
![Semantic description of image](/fromday3/setall.PNG )

After running synthesis, we check the area and timing again:
![Semantic description of image](/fromday3/newarea.PNG )
![Semantic description of image](/fromday3/newtiming.PNG )
As we see, Area has increased with improved timing. This is an iterative process and is carried out till we get a positive slack.


Lets run the floorplan and placement to check our new design inclusion into the design:
![Semantic description of image](/fromday3/readplacement.PNG )

Here we see our design included into the placement.
![Semantic description of image](/fromday3/sky130vsd.PNG )

Expanded version:

![Semantic description of image](/fromday3/expandedver.PNG )


### Using STA to solve the timing violations
STA tool is used along with pre_base.sdc and pre_sta.conf files to solve the slack violations.
![Semantic description of image](/fromday3/mybasesdc.PNG )
![Semantic description of image](/fromday3/pre_const.PNG )

To invoke OpenSTA:
![Semantic description of image](/fromday3/starun.PNG )

Result as before:
![Semantic description of image](/fromday3/afterstaconf.PNG )

Checking the report shows we have high fanouts which causes high slew and results in timing violations. Need to reduce it to 4 and re-run the synthesis:
![Semantic description of image](/fromday3/maxfanout.PNG )

You can check the drivers for the nets with 6 fanouts:
![Semantic description of image](/fromday3/12904reason.PNG )
![Semantic description of image](/fromday3/12904.PNG )

set the max fanout and re-run the synthesis
![Semantic description of image](/fromday3/maxfancmd.PNG )

Latest slack:

![Semantic description of image](/fromday3/iteration.PNG )


You can also replace cells with high drive strengths to reduce the delay:
![Semantic description of image](/fromday3/report_conn.PNG )

Latest slack:
![Semantic description of image](/fromday3/newslack.PNG )

In this way its carried out till we get a positive slack. In the industry tools, all these steps are automated and the tools itself will try to match the timing requirements by using different cells from the library.

**write_verilog** command is used to output the improved netlist from the OpenSTA tool.

You can also check whether your replaced cells is visible in the synthesized netlist:
Latest slack:
![Semantic description of image](/fromday3/buf8.PNG )

### CTS
In this step, we use the placement file generated from the above synthesis file and run the CTS triton tool to insert Clock tree into the design.

Clock tree is very important to avoid clock skews(different arrival time of clocks) and also Delay inclusions due to Capacitive coupling through other nets.

If Clock skew is not zero, then different registers might store the data at different times which results in wrong functionality. This is solved by generating an H tree like connections for all registers.
H-Tree: an algorithm which finds the approximate mid point for all the flops and builds a clock tree from there. This helps to minimize the skew close to zero. Clock Buffers(rise and fall times are same) are added to maintain the signal integrity and no slews in the signal.

Delay inclusions can be avoided if the clock routes doesn't interact with the other routes. This is done by clock net shielding which reduces the capacitive coupling.

CTS variables can be checked in README.md
![Semantic description of image](/fromday3/CTSvariables.PNG )

After running cts using **run_cts** command, we get a new synthesis file as shown:
![Semantic description of image](/fromday3/cts_syn.PNG )

You can also check the run_cts procedure to undertand what all the files it uses:
![Semantic description of image](/fromday3/run.PNG )

After performing CTS, OpenLANE will generate lef and def files which can also be viewed:
![Semantic description of image](/fromday3/lefread_latest.PNG )

![Semantic description of image](/fromday3/read_def.PNG )

Now to do the timing analysis in OpenROAD app after the latest lef and def files, we need to create a database. This database must be written everytime .def file changes. After writing, list of commands to be followed as shown:
![Semantic description of image](/fromday3/write_db.PNG )

![Semantic description of image](/fromday3/readmin.PNG )

After reporting the checks, we found that we had timing violations:

Hold slack:
![Semantic description of image](/fromday3/holdtime.PNG )

Setup slack:

![Semantic description of image](/fromday3/setuptime.PNG )

Hold violations are too critical as it very difficult to correct it unlike setup slack. The engineer needs to design precisely keeping all these into considerations.

The reason for the violations is that we had not linked the typical library before running checks and had to change:
![Semantic description of image](/fromday3/linktyplib.PNG )

Latest setup slack:
![Semantic description of image](/fromday3/lastsetup.PNG )


### Using synthesis file given and to check slack variations when buffer list is changed

Lets use the synthesis file provided:
![Semantic description of image](/fromday3/newsyn.PNG )

Slack info:
![Semantic description of image](/fromday3/newslack.PNG )

Now lets run--> floorplan --> placement --> cts

Lets change the buffer list and run to check the slack:
![Semantic description of image](/fromday3/replace.PNG )

After changing, we run again. The cts doesn't run and the process needs to be killed due to current def not changed to placement .def:
![Semantic description of image](/fromday3/kill.PNG )

Change the current .def
![Semantic description of image](/fromday3/run_cts.PNG )

Now openROAD and set all the required variables and check the report:
![Semantic description of image](/fromday3/lastrun.PNG )
![Semantic description of image](/fromday3/lastrun1.PNG )

Latest setup slack:
![Semantic description of image](/fromday3/lastestslack.PNG )

Latest hold slack:
![Semantic description of image](/fromday3/latest_hold.PNG )

You can also check on skew:
![Semantic description of image](/fromday3/skewlast.PNG )

replace the Buffer list back before going to next steps:
![Semantic description of image](/fromday3/replaceback.PNG )



<a name="day5"></a>
## Day5 Final generation of RTL --> GDSII 

After the cts and timing analysis before, now we generate the power distribution network in OpenLANE. This step is always at the Floorplan in commercial tools. In OpenLANE higher versions, this has been moved to Floorplan step.

To generate PDN:
![Semantic description of image](/fromday3/genpdn.PNG )

It generates pdn.def file
![Semantic description of image](/fromday3/pdndef.PNG )

What the tool does is creation of power rings, halo, straps and rails as explained before above.

### Routing

This is the step where all the blocks/cells are interconnected using physical wires. Various routing algorithms are used to connect the cells. OpenLANE uses TritonRoute as the tool to carry out the routing.

Triton Tool operate in two stages:
1. Global Routing - Also called as Fast Routing. This creates a routing guide and not the actual routing. It defines the layers to be used and nets position in the chip.
2. Detailed Routing - Metal tracks are routed to connect the blocks using the guide created in the Global Routing step.

In general, two guides are connected if:
1. the guides on same layer.
2. the guides on neighbouring layer with non-zero vertical area.
3. Each unconnected pin are overlapped by route guide.

Also Intra Layer routing happens in parallel and inter layer happens one after another.

### Routing in OpenLANE:
Before running routing, you can check routing related variables in README.md:
![Semantic description of image](/fromday3/roting.PNG )

Triton Route has different routing strategies and higher the strategy the longer time it takes to route and vice versa. We ran with lower strategy and took around 10-12min with few DRC errors:
![Semantic description of image](/fromday3/drcvio.PNG )

These DRC errors can be manually fixed by the user or run with higher QoR setting of Triton.

### SPEF Extraction
After routing, parasitics are extracted to STA once again to confirm no timings violation because after this the designers cannot change anything as it goes to foundry for fab. The parasitics are extracted into a SPEF file and the SPEF extractor is used.
![Semantic description of image](/fromday3/spef.PNG )

.spef file:
![Semantic description of image](/fromday3/speffile.PNG )

### Contact
Pruthvi Raj Venkatesha: pru.11.93@gmail.com

### Acknowledgements
* Nickon jose- VSD VLSI Engineer
* Kunal Ghosh- Co-founder(VSD Corp. Pvt. Ltd)
* Praharsha- Member(VSD Corp. Pvt. Ltd)
* Akurathi Radhika-(VSD Corp. Pvt. Ltd)


